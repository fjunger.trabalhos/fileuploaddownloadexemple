package com.tests.test;

import org.junit.Test;
import static org.junit.Assert.*;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.runner.RunWith;
import static org.mockito.Mockito.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestApplicationTests {

	private static final HttpClient CLIENT = HttpClientBuilder
			.create()
			.build();
	
	@Test
	public void testFirstTest_expedtedOk() {
		assertEquals(1,1);
	}
	
	@Test
	public void testGetGoogleHomePage_expectedOk() throws ClientProtocolException, IOException {
		HttpGet httpGet = new HttpGet("http://www.google.com");
		
		HttpResponse response = CLIENT.execute(httpGet);
		
		assertTrue(response.getStatusLine().getStatusCode()==200);
	}
	
	@Test
	public void testParseGoogleDom_expectedOk() throws UnsupportedOperationException, SAXException, ParserConfigurationException, IOException {
		HttpGet httpGet = new HttpGet("http://www.google.com");
		
		HttpResponse response = CLIENT.execute(httpGet);
		
		HtmlPage htmlPage = new HtmlPage(EntityUtils.toString(response.getEntity()));
		String value = htmlPage.getValueByElementWithKeyValue("a","data-pid","23");
		
		assertEquals(value,"Gmail");
	}
	
	@Test
	public void testComoUsarMock() throws ClientProtocolException, IOException {
		HttpResponse mockResponse = mock(HttpResponse.class);

		Header mockHeader = mock(Header.class);
		
		when(mockResponse.getFirstHeader("Paulo"))
				.thenReturn(mockHeader);
		
		when(mockHeader.getName())
				.thenReturn("Leite");
		
		assertEquals(mockResponse.getFirstHeader("Paulo").getName(),"Leite");
	}

	
	@Test
	public void testHtmlPageGetElement() throws ClientProtocolException, IOException {
		String htmlPageString = "<html><body><p>valor1</p><p name=\"value\">valorCorreto</p></body></html>";
		
		HtmlPage htmlPage = new HtmlPage(htmlPageString);
		String value = htmlPage.getValueByElementWithKeyValue("p","name","value");
		
		assertEquals(value,"valorCorreto");
	}
	
	@Test
	public void testHtmlPageGetElement_withNameIncorrect_expetecNotOk() throws ClientProtocolException, IOException {
		String htmlPageString = "<html><body><p>valor1</p><p name1=\"value\">valorCorreto</p></body></html>";
		
		HtmlPage htmlPage = new HtmlPage(htmlPageString);
		String value = htmlPage.getValueByElementWithKeyValue("p","name","value");
		
		assertEquals(value,"");
	}
	
	@Test
	public void testHtmlPageGetElement_withValueIncorrect_expetecNotOk() throws ClientProtocolException, IOException {
		String htmlPageString = "<html><body><p>valor1</p><p name=\"value1\">valorCorreto</p></body></html>";
		
		HtmlPage htmlPage = new HtmlPage(htmlPageString);
		String value = htmlPage.getValueByElementWithKeyValue("p","name","value");
		
		assertEquals(value,"");
	}
}
