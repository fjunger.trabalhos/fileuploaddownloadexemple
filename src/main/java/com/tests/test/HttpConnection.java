package com.tests.test;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

public class HttpConnection {
	private HttpConnection() {}
	
	public static final HttpClient CLIENT = HttpClientBuilder
			.create()
			.build();
}
