package com.tests.test;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class HtmlPage {
	private Document rootDoc;
	
	public HtmlPage(String htmlPageString) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			rootDoc = builder.parse(new InputSource(new StringReader(htmlPageString)));
		} catch (SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public String getValueByElementWithKeyValue(String element, String key, String value) {
		NodeList nodeList = rootDoc.getElementsByTagName(element);
		
		for(int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			if(node.hasAttributes()) {
				Node keyValueNode = node.getAttributes().getNamedItem(key);
				if(keyValueNode != null) {
					if(keyValueNode.getNodeValue().equals(value)) {
						return node.getTextContent();
					}
				}
			}
		}

		return "";
	}

}
